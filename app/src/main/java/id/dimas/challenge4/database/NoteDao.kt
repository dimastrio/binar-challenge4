package id.dimas.challenge4.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface NoteDao {

    @Query("SELECT * FROM Note WHERE user_id = :userId")
    fun getAllNote(userId: Int?): List<Note>

    @Query("SELECT * FROM Note WHERE judul = :judul AND catatan = :catatan")
    fun getRegisteredNote(judul: String, catatan: String): List<Note>

    @Insert(onConflict = REPLACE)
    fun insertNote(note: Note): Long

    @Update
    fun updateNote(note: Note?): Int

    @Delete
    fun deleteNote(note: Note?): Int

}