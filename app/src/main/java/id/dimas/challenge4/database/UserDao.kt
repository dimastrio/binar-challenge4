package id.dimas.challenge4.database

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE

@Dao
interface UserDao {

    @Query("SELECT * FROM User")
    fun getAllUser(): List<User>

    @Query("SELECT * FROM User WHERE email = :email")
    fun getRegisteredUser(email: String): List<User>

    @Query("SELECT email FROM User WHERE email = :email")
    fun checkEmailUser(email: String): String

    @Query("SELECT * FROM User WHERE email =:email AND password =:password")
    fun checkRegisterUser(email: String, password: String): List<User>

    @Query("SELECT username FROM User WHERE email =:email")
    fun getUsername(email: String?): String

    @Query("SELECT userId FROM User WHERE email =:email")
    fun getUserId(email: String): Int?

    @Query("SELECT * FROM User")
    fun deleteAll(): List<User>

    @Insert(onConflict = REPLACE)
    fun insertUser(user: User): Long

    @Update
    fun updateUser(user: User): Int

    @Delete
    fun deleteUser(user: User): Int

}