package id.dimas.challenge4

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import id.dimas.challenge4.adapter.NoteActionListener
import id.dimas.challenge4.adapter.NoteAdapter
import id.dimas.challenge4.database.Note
import id.dimas.challenge4.database.NoteDatabase
import id.dimas.challenge4.databinding.FragmentHomeBinding
import id.dimas.challenge4.helper.NoteRepo
import id.dimas.challenge4.helper.SharedPref
import id.dimas.challenge4.helper.SharedPref.Companion.KEY_EMAIL
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private val binding get() = _binding!!

    private var mDb: NoteDatabase? = null
    private lateinit var noteRepo: NoteRepo
    private lateinit var noteAdapter: NoteAdapter

    private val sharedPref: SharedPref by lazy { SharedPref(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        noteRepo = NoteRepo(requireContext())
        mDb = NoteDatabase.getInstance(requireContext())
        initRecyclerView()
        getUsername()
        getDataFromDb()
        addNote()
        logout(view)
    }

//    private fun setUsername() {
//        val email = sharedPref.getEmail(KEY_EMAIL)
//        getUsername(email)
//    }

    private fun getUsername() {
        val email = sharedPref.getEmail(KEY_EMAIL)
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.userDao()?.getUsername(email)
            CoroutineScope(Dispatchers.Main).launch {
                val value = "Welcome, $result !"
                binding.tvWelcome.text = value
            }
        }
    }


    private fun logout(view: View) {
        val btnLogout = view.findViewById<Button>(R.id.btn_logout)
        btnLogout.setOnClickListener {
            sharedPref.clearPref()
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToLoginFragment())
        }
    }

    private fun initRecyclerView() {
        binding.apply {
            noteAdapter = NoteAdapter(action)
            rvData.adapter = noteAdapter
            rvData.layoutManager = LinearLayoutManager(requireContext())
        }
    }

    private fun addNote() {
        showDialogInput()
    }

    private val action = object : NoteActionListener {
        override fun onDelete(note: Note) {
            showDialogDelete(note)
        }

        override fun onEdit(note: Note) {
            showDialogEdit(note)
        }

    }

    private fun getDataFromDb() {
        val userId = sharedPref.getUserId()
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.getAllNote(userId)
            if (result != null) {
                CoroutineScope(Dispatchers.Main).launch {
                    noteAdapter.updateData(result)
                }
            }
        }
    }

    private fun showDialogInput() {
        binding.fabAdd.setOnClickListener {
            val customLayout =
                LayoutInflater.from(requireContext())
                    .inflate(R.layout.layout_alert_input, null, false)

            val etJudul = customLayout.findViewById<EditText>(R.id.et_judul)
            val etCatatan = customLayout.findViewById<EditText>(R.id.et_catatan)
            val btnInput = customLayout.findViewById<Button>(R.id.btn_input)

            val builder = AlertDialog.Builder(requireContext())

            builder.setView(customLayout)

            val dialog = builder.create()

            btnInput.setOnClickListener {
                CoroutineScope(Dispatchers.IO).launch {
                    val judul = etJudul.text.toString()
                    val catatan = etCatatan.text.toString()
                    val userId = sharedPref.getUserId()
                    val newNote = Note(null, userId, judul, catatan)

                    val result = mDb?.noteDao()?.insertNote(newNote)
                    if (result != 0L) {
                        getDataFromDb()
                        showToastMainThread("Sukses Ditambahkan")
                    } else {
                        showToastMainThread("Gagal Ditambahkan")
                    }
                }
                dialog.dismiss()
            }

            dialog.show()
        }
    }

    private fun showDialogDelete(note: Note?) {
        val customLayout =
            LayoutInflater.from(requireContext()).inflate(R.layout.layout_alert_delete, null, false)

        val btnHapus = customLayout.findViewById<Button>(R.id.btn_delete)
        val btnCancel = customLayout.findViewById<Button>(R.id.btn_cancel)

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        btnHapus.setOnClickListener {
            CoroutineScope(Dispatchers.IO).launch {
                val result = mDb?.noteDao()?.deleteNote(note)
                if (result != 0) {
                    getDataFromDb()
                    showToastMainThread("Berhasil Dihapus")
                } else {
                    showToastMainThread("Gagal Dihapus")
                }
            }
            dialog.dismiss()
        }

        btnCancel.setOnClickListener {
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun showDialogEdit(note: Note?) {
        val customLayout =
            LayoutInflater.from(requireContext()).inflate(R.layout.layout_alert_edit, null, false)

        val etJudul = customLayout.findViewById<EditText>(R.id.et_judul)
        val etCatatan = customLayout.findViewById<EditText>(R.id.et_catatan)
        val btnEdit = customLayout.findViewById<Button>(R.id.btn_edit)


        if (note != null) {
            etJudul.setText(note.judul)
            etCatatan.setText(note.catatan)
        }

        val builder = AlertDialog.Builder(requireContext())

        builder.setView(customLayout)

        val dialog = builder.create()

        btnEdit.setOnClickListener {
            val judul = etJudul.text.toString()
            val catatan = etCatatan.text.toString()
            val userId = sharedPref.getUserId()

            if (judul.isEmpty()) {
                etJudul.error = "Judul Tidak Boleh Kosong"
            } else if (catatan.isEmpty()) {
                etCatatan.error = "Catatan Tidak Boleh Kosong"
            } else {
                val newNote = Note(note?.id, userId, judul, catatan)
                updateToDb(newNote)
            }

            dialog.dismiss()
        }
        dialog.show()
    }

    private fun updateToDb(note: Note) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.noteDao()?.updateNote(note)
            if (result != 0) {
                getDataFromDb()
                showToastMainThread("Berhasil Diupdate")
            } else {
                showToastMainThread("Gagal Diupdate")
            }
        }
    }

//    private fun checkRegisteredNote(judul: String, catatan: String) {
//        CoroutineScope(Dispatchers.IO).launch {
//            val result = mDb?.noteDao()?.getRegisteredNote(judul, catatan)
//            if (!result.isNullOrEmpty()) {
//                showToastMainThread("Judul Atau Catatan Sudah Ada")
//            }
//        }
//    }

//    private fun showToast(message: String){
//        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
//    }

    private fun showToastMainThread(message: String) {
        CoroutineScope(Dispatchers.Main).launch {
            Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
        }
    }


}