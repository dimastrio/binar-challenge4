package id.dimas.challenge4

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import id.dimas.challenge4.database.NoteDatabase
import id.dimas.challenge4.database.User
import id.dimas.challenge4.databinding.FragmentRegisterBinding
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class RegisterFragment : Fragment() {

    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private var mDb: NoteDatabase? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = NoteDatabase.getInstance(requireContext())
        addUser()
    }

    private fun addUser() {
        binding.apply {
            btnRegister.setOnClickListener {

                val username = etUsername.text.toString()
                val email = etEmail.text.toString()
                val confPass = etConfPass.text.toString()
                val password = etPassword.text.toString()

                if (username.isEmpty()) {
                    etUsername.error = "Username tidak boleh kosong"
                } else if (email.isEmpty()) {
                    etEmail.error = "Email tidak boleh kosong"
                } else if (password.isEmpty()) {
                    etPassword.error = "Password tidak boleh kosong"
                } else if (confPass.isEmpty()) {
                    etConfPass.error = "Konfirmasi Password harus diisi"
                } else {
                    if (password == confPass) {
                        saveUserToDb(username, email, password)
                    } else {
                        etConfPass.error = "Konfirmasi Password Anda Tidak Sesuai"
                    }
                }
            }
        }

    }

    private fun saveUserToDb(username: String, email: String, password: String) {
        val user = User(null, username, email, password)
        CoroutineScope(Dispatchers.IO).launch {
            val emails = mDb?.userDao()?.checkEmailUser(email)
            if (emails == null) {
                val result = mDb?.userDao()?.insertUser(user)
                if (result != 0L) {
                    CoroutineScope(Dispatchers.Main).launch {
                        showToast("Register Sukses")
                        findNavController().popBackStack()
                    }
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    showToast("Email Sudah Terdaftar")
                }
            }
        }
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

//    private fun checkRegisterUser(email: String) {
//        CoroutineScope(Dispatchers.Main).launch {
//            val result = mDb?.userDao()?.getRegisteredUser(email)
//            if (!result.isNullOrEmpty()) {
//                showToastInMainThread("Email telah ada")
//            }
//        }
//    }


}