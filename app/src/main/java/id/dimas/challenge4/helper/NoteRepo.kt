package id.dimas.challenge4.helper

import android.content.Context
import id.dimas.challenge4.database.NoteDatabase
import id.dimas.challenge4.database.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class NoteRepo(context: Context) {

    private val mDb = NoteDatabase.getInstance(context)

    suspend fun getNote(user: User) = withContext(Dispatchers.IO) {
        mDb?.noteDao()?.getAllNote(userId = user.userId ?: 0)
    }


}