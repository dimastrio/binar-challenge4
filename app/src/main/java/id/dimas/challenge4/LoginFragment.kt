package id.dimas.challenge4

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.dimas.challenge4.database.NoteDatabase
import id.dimas.challenge4.databinding.FragmentLoginBinding
import id.dimas.challenge4.helper.NoteRepo
import id.dimas.challenge4.helper.SharedPref
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private lateinit var noteRepo: NoteRepo
    private var mDb: NoteDatabase? = null


    private val sharedPref: SharedPref by lazy { SharedPref(requireContext()) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mDb = NoteDatabase.getInstance(requireContext())
        noteRepo = NoteRepo(requireContext())
        loginFunc()
        moveToRegis(view)
    }

    private fun moveToRegis(view: View) {
        val tvRegister = view.findViewById<TextView>(R.id.tv_go_to_register)

        tvRegister.setOnClickListener {
            binding.etEmail.text.clear()
            binding.etPassword.text.clear()
            it.findNavController()
                .navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }
    }

    private fun checkUserFromDb(email: String, password: String) {
        CoroutineScope(Dispatchers.IO).launch {
            val result = mDb?.userDao()?.checkRegisterUser(email, password)
            if (!result.isNullOrEmpty()) {
                val userId = mDb?.userDao()?.getUserId(email)
                sharedPref.setData(email, userId!!)
                CoroutineScope(Dispatchers.Main).launch {
                    findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
                }
            } else {
                CoroutineScope(Dispatchers.Main).launch {
                    showToast("Username Atau Password Kamu Salah")
                }
            }
        }
    }

    private fun loginFunc() {
        binding.apply {
            btnLogin.setOnClickListener {
                val email = etEmail.text.toString()
                val password = etPassword.text.toString()
                when {
                    email.isEmpty() -> {
                        etEmail.error = "Email Tidak Boleh Kosong"
                    }
                    password.isEmpty() -> {
                        etPassword.error = "Password Tidak Boleh Kosong"
                    }
                    else -> {
                        checkUserFromDb(email, password)
                    }
                }
            }
        }

    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

}