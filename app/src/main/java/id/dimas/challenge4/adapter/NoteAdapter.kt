package id.dimas.challenge4.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import id.dimas.challenge4.R
import id.dimas.challenge4.database.Note

class NoteAdapter(private val listener: NoteActionListener) :
    RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    private val diffCallback = object : DiffUtil.ItemCallback<Note>() {
        override fun areItemsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Note, newItem: Note): Boolean {
            return oldItem.hashCode() == newItem.hashCode()
        }

    }

    private val differ = AsyncListDiffer(this, diffCallback)

    fun updateData(note: List<Note>) = differ.submitList(note)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {

        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return NoteViewHolder(view)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int = differ.currentList.size


    inner class NoteViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val tvJudul = view.findViewById<TextView>(R.id.tv_judul_value)
        val tvCatatan = view.findViewById<TextView>(R.id.tv_catatan_value)
        val btnDelete = view.findViewById<ImageView>(R.id.btn_delete)
        val btnEdit = view.findViewById<ImageView>(R.id.btn_edit)

        fun bind(note: Note) {
            tvJudul.text = note.judul
            tvCatatan.text = note.catatan

            btnDelete.setOnClickListener {
                listener.onDelete(note)
            }

            btnEdit.setOnClickListener {
                listener.onEdit(note)
            }
        }

    }
}

interface NoteActionListener {
    fun onDelete(note: Note)
    fun onEdit(note: Note)
}